const electron = require('electron')
// Module to control application life.
// const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow
const path = require('path')
const url = require('url')
const ipcMain = require('electron').ipcMain;
// var open = require("open");
// const shell = require('shell');

const {
	app,
	Menu,
  Tray
} = require('electron')
var open = require('open');

var iconPath = path.join(__dirname, '/assets/icons/256x256.png')
var appIcon = require('electron')

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow() {
	//Create the browser window.
	// if (process.platform === "linux") {
	//     appIcon = new Tray('assets/tray.png');
	// }
  tray = new Tray(path.join(__dirname, '/assets/tray.png'));
  tray.setToolTip('fleep chat');

	mainWindow = new BrowserWindow({
		width: 450,
		height: 800,
    icon: iconPath,
    show: false,
		webPreferences: {
			nodeIntegration: false,
			preload: path.join(__dirname, 'render.js')
		}
	})

	mainWindow.loadURL('https://fleep.io/chat');
	ipcMain.on('asynchronous-message', function(event, arg) {
		setBadge(arg);
	});

  mainWindow.once('ready-to-show', () => {
    mainWindow.show()
  })
	// Open the DevTools.
	//  mainWindow.webContents.openDevTools()

	// Emitted when the window is closed.
	mainWindow.on('closed', function() {
		// Dereference the window object, usually you would store windows
		// in an array if your app supports multi windows, this is the time
		// when you should delete the corresponding element.
		mainWindow = null
	})

  mainWindow.on('new-window', function(event, url){
    event.preventDefault();
    open(url);
  });

  tray.on('click', () => {
    mainWindow.isVisible() ? mainWindow.hide() : mainWindow.show()
  })

}

function setBadge(text) {
  if (app.dock && app.dock.setBadge) {
        app.dock.setBadge(text);
      } else if (app.setBadgeCount) {
        app.setBadgeCount(text.length ? (text.replace("+", "") / 1) : 0);
      }
// if (process.platform === "darwin") {
// 		app.dock.setBadge("" + text);
// 	} else
  if (process.platform === "win32") {
		var win = remote.getCurrentWindow();

		if (text === "") {
			win.setOverlayIcon(null, "");
			return;
		}
		// Create badge
		var canvas = document.createElement("canvas");
		canvas.height = 140;
		canvas.width = 140;
		var ctx = canvas.getContext("2d");
		ctx.fillStyle = "red";
		ctx.beginPath();
		ctx.ellipse(70, 70, 70, 70, 0, 0, 2 * Math.PI);
		ctx.fill();
		ctx.textAlign = "center";
		ctx.fillStyle = "white";

		if (text.length > 2) {
			ctx.font = "75px sans-serif";
			ctx.fillText("" + text, 70, 98);
		} else if (text.length > 1) {
			ctx.font = "100px sans-serif";
			ctx.fillText("" + text, 70, 105);
		} else {
			ctx.font = "125px sans-serif";
			ctx.fillText("" + text, 70, 112);
		}
		var badgeDataURL = canvas.toDataURL();
		var img = NativeImage.createFromDataUrl(badgeDataURL);
		win.setOverlayIcon(img, text);
	}
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function() {
	// On OS X it is common for applications and their menu bar
	// to stay active until the user quits explicitly with Cmd + Q
	if (process.platform !== 'darwin') {
		app.quit()
	}
})

app.on('activate', function() {
	// On OS X it's common to re-create a window in the app when the
	// dock icon is clicked and there are no other windows open.
	if (mainWindow === null) {
		createWindow()
	}
})

var AutoLaunch = require('auto-launch');

var fleepAutoLauncher = new AutoLaunch({
    name: 'Fleep',
    path: '/Applications/Fleep.app',
    isHidden: true,
});

fleepAutoLauncher.enable();

//fleepAutoLauncher.disable();


fleepAutoLauncher.isEnabled()
.then(function(isEnabled){
    if(isEnabled){
        return;
    }
    fleepAutoLauncher.enable();
})
.catch(function(err){
    // handle error
});
// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
