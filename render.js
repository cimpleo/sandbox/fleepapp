(function() {
    'use strict';
    const electron = require('electron')
    // Module to control application life.
    const app = electron.app
    var remote = electron.remote;

})(this);

const ipcRenderer = require('electron').ipcRenderer;

window.observer = new MutationObserver(function(mutation) {
    ipcRenderer.send('asynchronous-message', document.title.substring(document.title.lastIndexOf("(") + 1, document.title.lastIndexOf(")")));
});

window.onload = function() {
    var styleTag = $('<style>:root{min-width: 0;}</style>');
    $('html > head').append(styleTag);
    var element = document.getElementById('password');
    if (element != null && element.value == '') {
        document.getElementsByClassName('page-wrap')[0].style.minWidth = '100%';
        document.body.style.minWidth = '0px';
        document.getElementById('landing_header_area').style.display = 'none';
        document.getElementById('landing-footer-bg').style.display = 'none';
        document.styleSheets[1].addRule(".page-wrap:after", "content: none");
    }
    document.getElementsByTagName("pre").style = "white-space:pre-wrap";
    this.observer.observe(document.getElementsByTagName("title")[0], {
        childList: true,
        characterData: true,
        subtree: true
    });
};

function notifyMe(text) {
  // Let's check whether notification permissions have already been granted
  if (Notification.permission === "granted") {
    // If it's okay let's create a notification
    var notification = new Notification(text);
  }
  // Otherwise, we need to ask the user for permission
  else if (Notification.permission !== "denied") {
    Notification.requestPermission(function (permission) {
      // If the user accepts, let's create a notification
      if (permission === "granted") {
        var notification = new Notification(text);
      }
    });
  }

  // At last, if the user has denied notifications, and you
  // want to be respectful there is no need to bother them any more.
}
